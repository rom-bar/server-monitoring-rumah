package com.rombar.model

import java.util.*

/**
 * Created by Rommy akbar on 11/4/17.
 */

data class LevelTangki(val tanggal:Date, val nilai:Int)