package com.rombar

import com.rombar.model.LevelTangki
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseBody
import java.io.File
import java.io.FileNotFoundException
import java.util.*
import kotlin.collections.ArrayList

@Controller
class Controller {

    private val mapper = jacksonObjectMapper()
    private var kumpulanData: ArrayList<LevelTangki> = ArrayList<LevelTangki>()
    private var dataTerakhir = 0

    @GetMapping("/")
    internal fun index(model: MutableMap<String, Any>): String {
        val output = java.util.ArrayList<String>()
        if (kumpulanData.size == 0) {
            try {
                loadData()
            } catch (e: FileNotFoundException) {
            }
        }
        kumpulanData.forEach(
                fun(lvl: LevelTangki) {
                    val str = "Data pada waktu " + lvl.tanggal + " Ketinggian " + lvl.nilai + " cm"
                    output.add(str)
                }
        )
        model.put("records", output)
        return "index"
    }

    @PostMapping(value = "/simpanlevel", consumes = arrayOf("text/plain"))
    @ResponseBody
    internal fun postTankLevel(@RequestBody body: String): String {
//        println("Controller.postTankLevel -> " + body)
        var nilai = 0;
        try {
            nilai = Integer.valueOf(body);
        } catch (e: NumberFormatException) {
//            e.printStackTrace()
        }
        if (dataTerakhir != nilai)
            dataTerakhir = nilai;
        else
            return "SAME"
        try {

            if (kumpulanData.size == 0)
                loadData()
        } catch (e: FileNotFoundException) {
//            e.printStackTrace()
        } finally {
            val lvl = LevelTangki(Calendar.getInstance().time, nilai)

            kumpulanData.add(lvl)
            mapper.writeValue(File("data.json"), kumpulanData)
        }
        return "SUCCESS";
    }

    private fun loadData() {
        kumpulanData = mapper.readValue(File("data.json"))
    }
}